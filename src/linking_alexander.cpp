//============================================================================
// Name        : linking_alexander.cpp
// Author      : Guido Polles
// Version     : 0.1
// Copyright   : 
// Description : get the Alexander determinant for two rings
// Notes       : Requires the Eigen c++ library. 
//               Compile with --std=c++11
//============================================================================
#include <iostream>
#include <cstdio>
#include <fstream>
#include <list>
#include <algorithm>
#include <Eigen/Eigen>
#include <Eigen/Sparse>

#define INF 10000000

// Hardcoded variables, just to showcase some bad programming habits 
const float eps = 0.0001;
const float cell_size = 2.5; // should be larger than the longest bond


typedef Eigen::SparseMatrix<int> SpMat;
typedef Eigen::Vector3f vec3;
typedef std::vector<vec3 ,Eigen::aligned_allocator<vec3> > vvec;

typedef std::vector<int> ivec;
typedef std::list<int> ilist;

int sign(float a){
  return a>=0 ? 1: -1;
}

bool cross_on_xy(const vec3& p, const vec3& r, const vec3& q, const vec3& s,
                 float *t, float*u, float* z_distance)
{
  // p + t r = q + u s

  // t = (q - p) x s / (r x s)
  // u = (q - p) x r / (r x s)
  vec3 d = q-p;

  float cross = r[0]*s[1] - s[0]*r[1];
  if (cross == 0){
    std::cerr << "parallel segments detected" << std::endl;
    std::cerr << p << std::endl << q << std::endl << r << std::endl << s;
    return false;
  }
  *t = (d[0]*s[1] - s[0]*d[1]) / cross;
  *u = (d[0]*r[1] - r[0]*d[1]) / cross;

  if(*t >= 0 && *t < 1 && *u >= 0 && *u < 1){
    *z_distance = p[2] + (*t)*r[2] - (q[2] + (*u)*s[2]);
    return true;
  }

  return false;
}

void do_labelling(const ivec& z_index, // indicates whether chain1 passes over or under chain2
                  const ivec& chain1, // the ordered crossings indexes along chain1
                  const ivec& chain2, // the ordered crossings indexes along chain2
                  ivec& over_labels, // the labels of the overpassing arcs
                  ivec& in_labels // labels of inbound arcs
                  ){
  // for each crossing point, follows the corresponding chain in order to label
  // the arc starting from the point.
  int num_crossings = z_index.size();
  for (int i = 0; i < num_crossings; ++i){

    // we select the chain which passes underneath the current crossing
    const ivec& chain = (z_index[i] == -1) ? chain1: chain2;

    // find the current crossing on the selected chain
    int j = 0;
    while (i != chain[j])
      ++j;
    j = (j+1) % num_crossings;

    // now we follow the chain until the arc ends, labelling the overpassings
    while (z_index[chain[j]]*z_index[i] == -1){
      over_labels[chain[j]] = i;
      j = (j+1) % num_crossings;
    }

    // labels the inbound arc for the crossing
    in_labels[chain[j]] = i;
  }

}


// I need this stuff to order the crossings along the chain using stl::sort
// and keeping track of original indexes, don't want to reinvent the wheel
// I think it uses a heapsort, it should scale ~NlogN
typedef std::pair<int,float> mypair;
bool comparator ( const mypair& l, const mypair& r)  { return l.second < r.second; }

// gets the ccordinates of points, finds the crossings,
// z_index = 1 indicates that chain1 passes over chain2
// z_index = -1 vice versa
// in ordered_chain, the indexes of the crossings are
// stored in the order we encounter them following that chain
// c_type indicates the kind of crossing (left/right handed)
void find_crossings(const vvec& chain1,
                    const vvec& chain2,
                    ivec& ordered_chain1,
                    ivec& ordered_chain2,
                    ivec& z_index,
                    ivec& c_types){


  // create a grid on xy in order to scale better than O(N^2)
  float xmin=INF, xmax=-INF, ymin=INF, ymax=-INF;
  for (size_t i = 0; i < chain2.size(); ++i){
    xmin = std::min(xmin, chain2[i][0]);
    xmax = std::max(xmax, chain2[i][0]);
    ymin = std::min(ymin, chain2[i][1]);
    ymax = std::max(ymax, chain2[i][1]);
  }
  

  float box_size[2] = {xmax-xmin+eps, ymax-ymin+eps};

  int n_cell[2] = {int(box_size[0]/cell_size)+1, int(box_size[1]/cell_size)+1};
  
  std::vector<std::vector<std::list<int>  > >  grid(n_cell[0]);
  for (int i = 0; i < n_cell[0]; ++i){
    grid[i].resize(n_cell[1]);
  }

  for (size_t i = 0; i < chain2.size(); ++i){
    int ix = floor((chain2[i][0] - xmin) / cell_size);
    int iy = floor((chain2[i][1] - ymin) / cell_size);
    grid[ix][iy].push_back(i);
  }


  std::vector<mypair> crossings1;
  std::vector<mypair> crossings2;

  // reserve some space - dunno if it is a good guess
  crossings1.reserve(chain2.size());
  crossings2.reserve(chain2.size());
  z_index.reserve(chain2.size());
  z_index.reserve(chain2.size());

  int num_crossings = 0;
  for (size_t i = 0; i < chain1.size(); ++i){
    vec3 r = chain1[ (i+1) % chain1.size() ] - chain1[i];

    int ix = floor((chain1[i][0] - xmin) / cell_size);
    int iy = floor((chain1[i][1] - ymin) / cell_size);
    if( ix < -1 || ix > n_cell[0] ||
        iy < -1 || iy > n_cell[1]  )
      continue;

    for (int icell = ix-1; icell <= ix+1; ++icell){
      if (icell < 0 || icell >= n_cell[0]) continue;
      for (int jcell = iy-1; jcell <= iy+1; ++jcell){
        if (jcell < 0 || jcell >= n_cell[1]) continue;
        for (int j : grid[icell][jcell]){
          vec3 s = chain2[ (j+1) % chain2.size() ] - chain2[j];
          float t, u, z;
          if ( cross_on_xy(chain1[i], r, chain2[j], s, &t, &u, &z) ){
            // t and u are between 0 and 1 , so if we sort on i+t
            // we get an ordering along the chain
            crossings1.push_back(mypair(num_crossings, i+t));
            crossings2.push_back(mypair(num_crossings, j+u));
            z_index.push_back(sign(z));
            float c_type = r[0]*s[1] - s[0]*r[1];
            c_types.push_back( sign(c_type)*sign(z) );
            ++num_crossings;
          }
        }
      }
    }
  }

  // sort crossings along the chains
  std::sort(crossings1.begin(), crossings1.end(), comparator);
  std::sort(crossings2.begin(), crossings2.end(), comparator);

  ordered_chain1.resize(num_crossings);
  ordered_chain2.resize(num_crossings);
  for (int i = 0; i < num_crossings; ++i) {
    ordered_chain1[i] = crossings1[i].first;
    ordered_chain2[i] = crossings2[i].first;
  }
}




using namespace std;
using namespace Eigen;

int main(int argc, char* argv[]) {
  // reads the two chains
  vvec chain1, chain2;
  float x,y,z;
  int len;
  ifstream fin(argv[1]);
  fin >> len;
  chain1.reserve(len);
  while (!(fin >> x >> y >> z).fail()){
    chain1.push_back(vec3(x, y, z));
  }
  fin.close();
  fin.open(argv[2]);
  fin >> len;
  chain2.reserve(len);
  while (!(fin >>x >> y >> z).fail()){
    chain2.push_back(vec3(x, y, z));
  }
  chain1.pop_back();
  chain2.pop_back();


  // find crossings, order them on the chains, and get the kind of crossing
  // z_indexes[i] = 1 if chain1 over chain2, -1 else.
  // c_types[i] tell if the crossing is right or left handed, i.e. sign( v1 ^ v2 )
  ivec ordered_crossings1, ordered_crossings2;
  ivec z_indexes, c_types;
  find_crossings(chain1, chain2, ordered_crossings1, ordered_crossings2, z_indexes, c_types);
  int num_crossings = ordered_crossings1.size();

  cerr << num_crossings << " crossings." << endl;
  if(num_crossings == 0){
    cerr << "No crossings." << endl;
    cout << 0 << endl;
    return 0;
  }
  if(num_crossings % 2 == 1){
    cerr << "Odd number of crossings. What an odd situation!" << endl;
    cout << 0 << endl;
    return 1;
  }

  // check that not all the crossings are over / underpasses
  int z0 = z_indexes[0];
  bool ok = false;
  for (int i = 0; i < num_crossings; ++i) {
    if (z_indexes[i]*z0 == -1){
      ok = true;
      break;
    }
  }
  if (!ok){
    cerr << "All under or overcrossings (" << z0 << ")." << endl;
    cout << 0 << endl;
    return 0;
  }

  // two crossings, one up one down. obvious. And creates problems with a 2x2 matrix.
  if (num_crossings == 2){
    cout << 1 << endl;
    return 0;
  }

  // labels the incoming and overpassing arcs
  ivec over_labels(num_crossings);
  ivec in_labels(num_crossings);
  do_labelling(z_indexes, ordered_crossings1, ordered_crossings2, over_labels, in_labels);
  for (int i = 0; i < num_crossings; ++i){
    // if a crossing is both the starting and the ending point
    // of one arc, the two curves are simply linked
    if (in_labels[i] == i){
      cout << 1 << endl;
      return 0;
    }
  }

  // generate the alexander matrix
  // note that since we need column major for some reason we fill the
  // transposed matrix. We just want the determinant, so who cares.
  int dim = c_types.size()-1;
  SpMat A(dim,dim);
  A.reserve(VectorXi::Constant(dim,3));
  for (int i = 0; i < dim; ++i){
    A.insert(i, i) = c_types[i];
    if (over_labels[i] != dim)
      A.insert(over_labels[i], i) = -c_types[i]*2;
    if (in_labels[i] != dim)
      A.insert(in_labels[i], i) = c_types[i];
  }

  A.makeCompressed();
  // compute determinant with lu decomposition
  SparseLU<SpMat, COLAMDOrdering<int> > lu;
  lu.analyzePattern(A);
  lu.factorize(A);
  float det = lu.absDeterminant();

  cout << det << endl;
  return 0;

}